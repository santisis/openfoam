dx = .5;
dy = .5;
dz = .2;


cd = .5;
cw = 4;

z0 = 9.1;
z3 = 9.17;

s8 = Sin(Pi/8);
c8 = Cos(Pi/8);

cd8 = cd / c8;
cw8 = cw / c8;

Point(1) = {0, 0, z0, 1.0};
Translate {0, 17, (z3-z0)*17/(17+47+7)} {
  Duplicata { Point{1}; }

}
Translate {47*Sin(Pi/4), 47*Cos(Pi/4), (z3-z0)*47/(17+47+7)} {
  Duplicata { Point{2}; }
}
Translate {17-20/2, 0, (z3-z0)*7/(17+47+7)} {
  Duplicata { Point{3}; }
}

Translate {cd/2, 0, 0} {
  Duplicata { Point{1}; }
}
Translate {cw, 0, 0} {
  Duplicata { Point{5}; }
}
Translate {-cd/2, 0, 0} {
  Duplicata { Point{1}; }
}
Translate {-cw, 0, 0} {
  Duplicata { Point{7}; }
}
Translate {cd8/2*Cos(Pi/8), -cd8/2*Sin(Pi/8), 0} {
  Duplicata { Point{2}; }
}
Translate {cw8*Cos(Pi/8), -cw8*Sin(Pi/8), 0} {
  Duplicata { Point{9}; }
}
Translate {-cd8/2*Cos(Pi/8), cd8/2*Sin(Pi/8), 0} {
  Duplicata { Point{2}; }
}
Translate {-cw8*Cos(Pi/8), cw8*Sin(Pi/8), 0} {
  Duplicata { Point{11}; }
}
Translate {cd8/2*Sin(Pi/8), -cd8/2*Cos(Pi/8), 0} {
  Duplicata { Point{3}; }
}
Translate {cw8*Sin(Pi/8), -cw8*Cos(Pi/8), 0} {
  Duplicata { Point{13}; }
}
Translate {-cd8/2*Sin(Pi/8), cd8/2*Cos(Pi/8), 0} {
  Duplicata { Point{3}; }
}
Translate {-cw8*Sin(Pi/8), cw8*Cos(Pi/8), 0} {
  Duplicata { Point{15}; }
}
Translate {0, cd/2, 0} {
  Duplicata { Point{4}; }
}
Translate {0, -cd/2, 0} {
  Duplicata { Point{4}; }
}
Translate {0, -cw, 0} {
  Duplicata { Point{18}; }
}
Translate {0, cw, 0} {
  Duplicata { Point{17}; }
}
Line(1) = {6, 10};
Line(2) = {10, 14};
Line(3) = {14, 19};
Line(4) = {19, 18};
Line(5) = {18, 13};
Line(6) = {13, 9};
Line(7) = {9, 5};
Line(8) = {5, 6};
Line(9) = {5, 7};
Line(10) = {7, 11};
Line(11) = {11, 15};
Line(12) = {15, 17};
Line(13) = {17, 20};
Line(14) = {20, 16};
Line(15) = {16, 12};
Line(16) = {12, 8};
Line(17) = {8, 7};
Line(18) = {18, 17};
Line(31) = {10, 9};
Line(32) = {11, 12};
Line(33) = {14, 13};
Line(34) = {15, 16};

Transfinite Line {13, 4, 34, 33, 32, 31, 17, 8} = (cw / dy + 1) Using Progression 1;
Transfinite Line {9, 18} = (cd / dx + 1) Using Progression 1;

Extrude {20, 0, 0} {
  Line{13, 18, 4};
  Layers{20 / dx + 1};
}
Transfinite Line {16, 10, 7, 1} = (17/dx + 1) Using Progression 1;
Transfinite Line {15, 11, 6, 2} = (47/dx + 1) Using Progression 1;
Transfinite Line {14, 12, 5, 3} = (7/dx + 1) Using Progression 1;
Line Loop(47) = {34, -14, -13, -12};
Plane Surface(48) = {47};
Line Loop(49) = {33, -5, -4, -3};
Plane Surface(50) = {49};
Line Loop(51) = {6, -31, 2, 33};
Plane Surface(52) = {51};
Line Loop(53) = {15, -32, 11, 34};
Plane Surface(54) = {53};
Line Loop(55) = {16, 17, 10, 32};
Plane Surface(56) = {55};
Line Loop(57) = {7, 8, 1, 31};
Plane Surface(58) = {57};

Extrude {0, -20, 0} {
  Line{17, 9, 8};
  Layers{20 / dy + 1};
}
Extrude {6, 0, 0} {
  Line{69};
  Layers{6 / dx + 1};
}
Extrude {-6, 0, 0} {
  Line{60};
  Layers{6 / dx + 1};
}

Transfinite Surface "*";
Recombine Surface "*";

Extrude {0, 0, 3.2} {
  Surface{78, 62, 66, 70, 74, 56, 58, 54, 52, 48, 50, 38, 42, 46};
  Layers{3.2 / dz + 1};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{100, 87, 122, 144, 166, 188};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, 0, -18.47-z0} {
  Surface{74, 70, 66, 62, 78};
  Layers{(-18.47-z0) / dz / 2 + 1};
  Recombine;
}
Extrude {0, 0, 13.5-3.2-z3} {
  Surface{342, 364, 386};
  Layers{(13.5-3.2-z3)/dz + 1};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{694, 672, 650};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, .2, 0} {
  Surface{751};
  Layers{.2 / dy + 1};
  Recombine;
}
Extrude {0, 4, 0} {
  Surface{782};
  Layers{4 / dy + 1};
  Recombine;
}
Extrude {0, 0, 10.1-13.5} {
  Surface{791};
  Layers{(10.1-13.5) / dz + 1};
  Recombine;
}

Physical Surface("inlet") = {628, 606, 584, 562, 540};
Physical Surface("atmosphere") = {518, 496, 474, 452, 408, 716, 738, 760, 777, 799};
Physical Surface("walls_camara") = {627, 593, 571, 549, 539, 99, 131, 187, 407, 439, 461, 483, 517, 623, 95, 403, 513, 183, 535, 531, 557, 579, 601, 619, 91, 117, 139, 161, 179, 509, 491, 469, 447, 399};
Physical Surface("walls_conductos") = {227, 232, 219, 58, 205, 210, 56, 197, 241, 54, 254, 249, 263, 276, 52, 271, 289, 298, 48, 297, 320, 311, 50, 319};
Physical Surface("walls_anulado") = {381, 337};
Physical Surface("walls_vertedero") = {769};
Physical Surface("walls_vertidor") = {385, 693, 715, 681, 659, 637, 703, 725, 747, 781, 803, 641, 804, 795, 773, 755, 733, 711, 689, 667, 645, 817, 825, 38, 42, 46, 351, 359, 333, 813};
Physical Surface("outlet") = {826, 821};
Physical Volume("mesh") = {33, 32, 31, 30, 29, 34, 26, 27, 28, 14, 13, 12, 11, 10, 9, 8, 7, 6, 15, 17, 18, 19, 20, 5, 4, 3, 2, 1, 25, 24, 23, 22, 21};
Delete {
  Volume{16};
}
Delete {
  Surface{417};
}
Delete {
  Surface{430};
}
Delete {
  Line{410};
}
Delete {
  Point{177};
}
Delete {
  Point{176};
}
