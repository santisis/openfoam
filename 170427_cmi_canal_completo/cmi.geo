dx = .5;
dy = .5;
dz = .2;


cd = .5;
cw = 4;

z0 = 9.1;
z3 = 9.17;

s8 = Sin(Pi/8);
c8 = Cos(Pi/8);

cd8 = cd / c8;
cw8 = cw / c8;

Point(1) = {0, 0, z0, 1.0};
Translate {0, 17, (z3-z0)*17/(17+47+7)} {
  Duplicata { Point{1}; }

}
Translate {47*Sin(Pi/4), 47*Cos(Pi/4), (z3-z0)*47/(17+47+7)} {
  Duplicata { Point{2}; }
}
Translate {17-20/2, 0, (z3-z0)*7/(17+47+7)} {
  Duplicata { Point{3}; }
}

Translate {cd/2, 0, 0} {
  Duplicata { Point{1}; }
}
Translate {cw, 0, 0} {
  Duplicata { Point{5}; }
}
Translate {-cd/2, 0, 0} {
  Duplicata { Point{1}; }
}
Translate {-cw, 0, 0} {
  Duplicata { Point{7}; }
}
Translate {cd8/2*Cos(Pi/8), -cd8/2*Sin(Pi/8), 0} {
  Duplicata { Point{2}; }
}
Translate {cw8*Cos(Pi/8), -cw8*Sin(Pi/8), 0} {
  Duplicata { Point{9}; }
}
Translate {-cd8/2*Cos(Pi/8), cd8/2*Sin(Pi/8), 0} {
  Duplicata { Point{2}; }
}
Translate {-cw8*Cos(Pi/8), cw8*Sin(Pi/8), 0} {
  Duplicata { Point{11}; }
}
Translate {cd8/2*Sin(Pi/8), -cd8/2*Cos(Pi/8), 0} {
  Duplicata { Point{3}; }
}
Translate {cw8*Sin(Pi/8), -cw8*Cos(Pi/8), 0} {
  Duplicata { Point{13}; }
}
Translate {-cd8/2*Sin(Pi/8), cd8/2*Cos(Pi/8), 0} {
  Duplicata { Point{3}; }
}
Translate {-cw8*Sin(Pi/8), cw8*Cos(Pi/8), 0} {
  Duplicata { Point{15}; }
}
Translate {0, cd/2, 0} {
  Duplicata { Point{4}; }
}
Translate {0, -cd/2, 0} {
  Duplicata { Point{4}; }
}
Translate {0, -cw, 0} {
  Duplicata { Point{18}; }
}
Translate {0, cw, 0} {
  Duplicata { Point{17}; }
}
Line(1) = {6, 10};
Line(2) = {10, 14};
Line(3) = {14, 19};
Line(4) = {19, 18};
Line(5) = {18, 13};
Line(6) = {13, 9};
Line(7) = {9, 5};
Line(8) = {5, 6};
Line(9) = {5, 7};
Line(10) = {7, 11};
Line(11) = {11, 15};
Line(12) = {15, 17};
Line(13) = {17, 20};
Line(14) = {20, 16};
Line(15) = {16, 12};
Line(16) = {12, 8};
Line(17) = {8, 7};
Line(18) = {18, 17};
Line(31) = {10, 9};
Line(32) = {11, 12};
Line(33) = {14, 13};
Line(34) = {15, 16};

Transfinite Line {13, 4, 34, 33, 32, 31, 17, 8} = (cw / dy + 1) Using Progression 1;
Transfinite Line {9, 18} = (cd / dx + 1) Using Progression 1;

Extrude {20, 0, 0} {
  Line{13, 18, 4};
  Layers{20 / dx + 1};
}
Transfinite Line {16, 10, 7, 1} = (17/dx + 1) Using Progression 1;
Transfinite Line {15, 11, 6, 2} = (47/dx + 1) Using Progression 1;
Transfinite Line {14, 12, 5, 3} = (7/dx + 1) Using Progression 1;
Line Loop(47) = {34, -14, -13, -12};
Plane Surface(48) = {47};
Line Loop(49) = {33, -5, -4, -3};
Plane Surface(50) = {49};
Line Loop(51) = {6, -31, 2, 33};
Plane Surface(52) = {51};
Line Loop(53) = {15, -32, 11, 34};
Plane Surface(54) = {53};
Line Loop(55) = {16, 17, 10, 32};
Plane Surface(56) = {55};
Line Loop(57) = {7, 8, 1, 31};
Plane Surface(58) = {57};

Extrude {0, -20, 0} {
  Line{17, 9, 8};
  Layers{20 / dy + 1};
}
Extrude {6, 0, 0} {
  Line{69};
  Layers{6 / dx + 1};
}
Extrude {-6, 0, 0} {
  Line{60};
  Layers{6 / dx + 1};
}

Transfinite Surface "*";
Recombine Surface "*";

Extrude {0, 0, 3.2} {
  Surface{78, 62, 66, 70, 74, 56, 58, 54, 52, 48, 50, 38, 42, 46};
  Layers{3.2 / dz + 1};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{100, 87, 122, 144, 166, 188};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, 0, -18.47-z0} {
  Surface{74, 70, 66, 62, 78};
  Layers{(-18.47-z0) / dz / 2 + 1};
  Recombine;
}
Extrude {0, 0, 13.5-3.2-z3} {
  Surface{342, 364, 386};
  Layers{(13.5-3.2-z3)/dz + 1};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{694, 672, 650};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, .2, 0} {
  Surface{751};
  Layers{.2 / dy + 1};
  Recombine;
}
Extrude {0, 4, 0} {
  Surface{782};
  Layers{4 / dy + 1};
  Recombine;
}
Extrude {0, 0, 10.1-13.5} {
  Surface{791};
  Layers{(10.1-13.5) / dz + 1};
  Recombine;
}

Delete {
  Volume{16};
}
Delete {
  Surface{417};
}
Delete {
  Surface{430};
}
Delete {
  Line{410};
}
Delete {
  Point{177};
}
Delete {
  Point{176};
}
Extrude {344, 0, .33} {
  Surface{381, 337};
  Layers{344 / dx + 1};
  Recombine;
}
Line(871) = {347, 353};
Line(872) = {338, 349};
Transfinite Line {871, 872} = cd / dy +1 Using Progression 1;
Line Loop(873) = {871, -851, -872, -831};
Plane Surface(874) = {873};
Transfinite Surface 874;
Extrude {1, 0, 0} {
  Surface{848, 874, 870};
  Layers{1 / dx};
  Recombine;
}
Extrude {0, 0, 20} {
  Surface{891, 905, 935};
  Layers{20 / dz + 1};
  Recombine;
}

Physical Surface("atmosphere") = {408, 452, 474, 496, 518, 716, 738, 760, 777, 799, 962, 984, 1006};
Physical Surface("inlet") = {540, 562, 584, 606, 628};
Physical Surface("outlet") = {826};
Physical Volume("mesh") = {15, 17, 18, 19, 20, 1, 3, 4, 2, 5, 25, 24, 22, 23, 21, 6, 7, 8, 9, 10, 11, 33, 32, 30, 31, 29, 14, 12, 34, 13, 27, 26, 28, 35, 36, 37, 38, 39, 40, 42, 41};
