//c01 = 17.3;
c01 = 23;
c00 = -18.47;
c0 = 9.08;
r0 = 16.5;
hc = 3.2;
l1 = 20;
l0 = 276-l1/2;
c1 = 9.21;
l2 = 171-l1/2;
c2 = 9.3;
cv = 13.5;
wc = 4;
cci = 10.63;
ccf = 10.88;
cl = 500;

dx = .5;
dy = .5;
dz = .2;

Point(1) = {0, 0, c0, 1.0};

Extrude {-r0, 0, 0} {
  Point{1};
  Layers{r0 / dx + 1};
  Recombine;
}

Extrude {l0, 0, c1-c0} {
  Point{1};
  Layers{l0 / dx};
  Recombine;
}

Extrude {(l1-wc)/2, 0, 0} {
  Point{3};
  Layers{(l1-wc)/2 / dx};
  Recombine;
}
Extrude {wc, 0, 0} {
  Point{4};
  Layers{wc / dx};
  Recombine;
}
Extrude {(l1-wc)/2, 0, 0} {
  Point{5};
  Layers{(l1-wc)/2 / dx};
  Recombine;
}
Extrude {l2, 0, c2-c1} {
  Point{6};
  Layers{l2 / dx};
  Recombine;
}
Extrude {0, 0, hc} {
  Line{1, 2, 4, 3, 5, 6};
  Layers{hc / dz};
  Recombine;
}
Extrude {0, wc, 0} {
  Surface{10, 14, 22, 18, 26, 30};
  Layers{wc / dy};
  Recombine;
}
Extrude {0, 0, cv-hc-c1} {
  Surface{91, 113, 135};
  Layers{(cv-hc-c1) / dz};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{184, 206, 228};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, .2, 0} {
  Surface{245, 267, 289};
  Layers{Ceil(.2 / dy)};
  Recombine;
}
Extrude {0, 4, 0} {
  Surface{360, 338, 316};
  Layers{4 / dy};
  Recombine;
}
Extrude {0, 0, -.2} {
  Surface{369, 391, 413};
  Layers{Ceil(.2 / dz)};
  Recombine;
}
Extrude {0, 0, -hc} {
  Surface{448, 470, 492};
  Layers{hc / dz};
  Recombine;
}
Extrude {0, cl-8, ccf - cci} {
  Surface{531};
  Layers{(cl-8) / dy};
  Recombine;
}
Extrude {0, -(r0-wc)/2, 0} {
  Surface{10};
  Layers{(r0-wc)/2 / dy};
  Recombine;
}
Extrude {0, (r0-wc)/2, 0} {
  Surface{52};
  Layers{(r0-wc)/2 / dy};
  Recombine;
}
Extrude {0, 0, c00-c0} {
  Surface{589, 39, 611};
  Layers{(c00-c0) / dz / 2};
  Recombine;
}
Extrude {0, 0, c01-(hc+c0)} {
  Surface{597, 47, 619};
  Layers{(c01-(hc+c0)) / dz * 5};
  Recombine;
}

Extrude {0, 4, 0} {
  Surface{580};
  Layers{4/dy};
  Recombine;
}
Extrude {0, 0, 15.9-9.39-3.2-.2 -.8} {
  Surface{765};
  Layers{(15.9-9.39-3.2-.2 - .8) / dz};
  Recombine;
}

Extrude {0, 0, 10} {
  Surface{800};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {(l1-wc)/2, 0, 0} {
  Surface{821, 799, 777};
  Layers{(l1 - wc) / 2 / dx};
  Recombine;
}

Extrude {-(l1-wc)/2, 0, 0} {
  Surface{813, 791, 769};
  Layers{(l1 - wc) / 2 / dx};
  Recombine;
}

Extrude {0, .2, 0} {
  Surface{901, 817, 843};
  Layers{Ceil(.2 / dy)};
  Recombine;
}
Extrude {0, 4, 0} {
  Surface{976, 998, 1020};
  Layers{4 / dy};
  Recombine;
}

Extrude {1, 0, 0} {
  Surface{153};
  Layers{1 / dx};
  Recombine;
}
Extrude {0, 0, 20} {
  Surface{1099};
  Layers{20 / dz};
  Recombine;
}

Extrude {0, 0, -2} {
  Surface{1077, 1051, 1041};
  Layers{2 / dz};
  Recombine;
}
Extrude {0, 0, -2} {
  Surface{1152, 1174, 1196};
  Layers{2 / dz};
  Recombine;
}

Delete {
  Volume{25, 40, 37, 43, 46, 56, 53, 41, 42, 44, 36, 47, 52, 55, 35, 34, 39, 38, 54, 51, 45, 48};
}
Delete {
  Surface{571, 575, 579, 567, 1033, 905, 1037, 910, 931, 954, 919, 932, 976, 909, 897, 809, 1173, 1196, 1029, 813, 817, 1064, 1059, 993, 1042, 1019, 1085, 989, 821, 1055, 839, 887, 857, 875, 580, 773, 765, 769, 787, 941, 945, 778, 800, 835, 1227, 985, 1161, 1209, 1152, 1011, 865, 883, 853, 831, 888, 1147, 1077, 1213, 1143, 1086, 1081, 1020, 843, 822, 963, 1041, 923, 971, 975};
}
Delete {
  Surface{795, 1174, 1051, 998, 901, 1205, 777, 799, 1249, 949, 791, 1183, 1262, 1169, 1139, 1218, 844};
}
Delete {
  Surface{1240, 1217, 866, 1151, 1235, 1257, 1191, 1195, 1261, 1239};
}
Delete {
  Line{838, 1054, 804, 1022, 980, 1003, 816, 1234, 782, 562, 563, 560, 565, 574, 570, 566, 940, 561, 768, 917, 935, 934, 915, 895, 893, 890, 913, 808, 891, 802, 803, 904, 958, 957, 900, 1032, 892, 1024, 1023, 1028, 1046, 812, 1025, 1036, 1245, 959, 918, 781, 944, 764, 780, 786, 912, 936, 1242, 1176, 1181, 1179, 1247, 896, 956, 1178, 1027, 1044, 979, 805, 1050, 1002, 827, 825, 1220, 851, 1132, 829, 794, 783, 1154, 758, 830, 760, 847, 868, 852, 772, 873, 871, 870, 1204, 846, 1198, 882, 824, 1135, 1222, 1212, 1134, 1067, 1146, 1045, 1080, 1199, 1200, 1133, 1076, 1138, 849, 1001, 1201, 978, 807, 759, 1225, 790, 1159, 1256, 1190, 834, 1157, 1244, 1223, 1168, 1156, 826, 1137, 1203, 761, 763, 785, 1068, 1069, 1142, 1208};
}
Delete {
  Surface{967, 1015};
}
Delete {
  Line{957, 900, 827, 1002};
}
Physical Surface("atmosphere") = {1130, 250, 421, 399, 377, 294, 272, 355, 333, 311, 712, 734, 756};
Physical Surface("inlet") = {668, 690, 646};
Physical Surface("outlet") = {558, 536, 514};
Physical Volume("mesh") = {10, 13, 18, 11, 7, 3, 24, 21, 14, 17, 16, 15, 20, 12, 8, 4, 23, 9, 5, 22, 19, 6, 49, 50, 28, 29, 30, 2, 26, 1, 27, 33, 32, 31};
