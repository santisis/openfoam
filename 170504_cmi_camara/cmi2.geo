dx = .5;
dy = .5;
dz = .2;

dc = 2 / dy + 2;


cd = .5;
cw = 4;

z0 = 9.1;
z3 = 9.17;

s8 = Sin(Pi/8);
c8 = Cos(Pi/8);

cd8 = cd / c8;
cw8 = cw / c8;

Point(1) = {0, 0, z0, 1.0};
Translate {0, 17, (z3-z0)*17/(17+47+7)} {
  Duplicata { Point{1}; }

}
Translate {47*Sin(Pi/4), 47*Cos(Pi/4), (z3-z0)*47/(17+47+7)} {
  Duplicata { Point{2}; }
}
Translate {17-20/2, 0, (z3-z0)*7/(17+47+7)} {
  Duplicata { Point{3}; }
}

Translate {cd/2, 0, 0} {
  Duplicata { Point{1}; }
}
Translate {cw, 0, 0} {
  Duplicata { Point{5}; }
}
Translate {-cd/2, 0, 0} {
  Duplicata { Point{1}; }
}
Translate {-cw, 0, 0} {
  Duplicata { Point{7}; }
}
Translate {cd8/2*Cos(Pi/8), -cd8/2*Sin(Pi/8), 0} {
  Duplicata { Point{2}; }
}
Translate {cw8*Cos(Pi/8), -cw8*Sin(Pi/8), 0} {
  Duplicata { Point{9}; }
}
Translate {-cd8/2*Cos(Pi/8), cd8/2*Sin(Pi/8), 0} {
  Duplicata { Point{2}; }
}
Translate {-cw8*Cos(Pi/8), cw8*Sin(Pi/8), 0} {
  Duplicata { Point{11}; }
}
Translate {cd8/2*Sin(Pi/8), -cd8/2*Cos(Pi/8), 0} {
  Duplicata { Point{3}; }
}
Translate {cw8*Sin(Pi/8), -cw8*Cos(Pi/8), 0} {
  Duplicata { Point{13}; }
}
Translate {-cd8/2*Sin(Pi/8), cd8/2*Cos(Pi/8), 0} {
  Duplicata { Point{3}; }
}
Translate {-cw8*Sin(Pi/8), cw8*Cos(Pi/8), 0} {
  Duplicata { Point{15}; }
}
Translate {0, cd/2, 0} {
  Duplicata { Point{4}; }
}
Translate {0, -cd/2, 0} {
  Duplicata { Point{4}; }
}
Translate {0, -cw, 0} {
  Duplicata { Point{18}; }
}
Translate {0, cw, 0} {
  Duplicata { Point{17}; }
}
Line(1) = {6, 10};
Line(2) = {10, 14};
Line(3) = {14, 19};
Line(4) = {19, 18};
Line(5) = {18, 13};
Line(6) = {13, 9};
Line(7) = {9, 5};
Line(8) = {5, 6};
Line(9) = {5, 7};
Line(10) = {7, 11};
Line(11) = {11, 15};
Line(12) = {15, 17};
Line(13) = {17, 20};
Line(14) = {20, 16};
Line(15) = {16, 12};
Line(16) = {12, 8};
Line(17) = {8, 7};
Line(18) = {18, 17};
Line(31) = {10, 9};
Line(32) = {11, 12};
Line(33) = {14, 13};
Line(34) = {15, 16};

Transfinite Line {13, 4, 34, 33, 32, 31, 17, 8} = (cw / dy + 1) Using Progression 1;
Transfinite Line {9, 18} = (cd / dx + 1) Using Progression 1;

Extrude {20, 0, 0} {
  Line{13, 18, 4};
  Layers{20 / dx + 1};
}
Transfinite Line {16, 10, 7, 1} = (17/dx + 1) Using Progression 1;
Transfinite Line {15, 11, 6, 2} = (47/dx + 1) Using Progression 1;
Transfinite Line {14, 12, 5, 3} = (7/dx + 1) Using Progression 1;
Line Loop(47) = {34, -14, -13, -12};
Plane Surface(48) = {47};
Line Loop(49) = {33, -5, -4, -3};
Plane Surface(50) = {49};
Line Loop(51) = {6, -31, 2, 33};
Plane Surface(52) = {51};
Line Loop(53) = {15, -32, 11, 34};
Plane Surface(54) = {53};
Line Loop(55) = {16, 17, 10, 32};
Plane Surface(56) = {55};
Line Loop(57) = {7, 8, 1, 31};
Plane Surface(58) = {57};

Extrude {0, -2, 0} {
  Line{17, 9, 8};
  Layers{dc - 1};
}
Extrude {1, 0, 0} {
  Line{69};
  Layers{1 / dx + 1};
}
Extrude {-3.44, 0, 0} {
  Line{60};
  Layers{3.44 / dx + 1};
}

Transfinite Surface "*";
Recombine Surface "*";

Extrude {0, 0, 3.2} {
  Surface{78, 62, 66, 70, 74, 56, 58, 54, 52, 48, 50, 38, 42, 46};
  Layers{3.2 / dz + 1};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{100, 87, 122, 144, 166, 188};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, 0, -18.47-z0} {
  Surface{74, 70, 66, 62, 78};
  Layers{(-18.47-z0) / dz / 2 + 1};
  Recombine;
}
Extrude {0, 0, 13.5-3.2-z3} {
  Surface{342, 364, 386};
  Layers{(13.5-3.2-z3)/dz + 1};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{694, 672, 650};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, .2, 0} {
  Surface{751};
  Layers{.2 / dy + 1};
  Recombine;
}
Extrude {0, 4, 0} {
  Surface{782};
  Layers{4 / dy + 1};
  Recombine;
}
Extrude {0, 0, 10.1-13.5} {
  Surface{791};
  Layers{(10.1-13.5) / dz + 1};
  Recombine;
}

Delete {
  Volume{16};
}
Delete {
  Surface{417};
}
Delete {
  Surface{430};
}
Delete {
  Line{410};
}
Delete {
  Point{177};
}
Delete {
  Point{176};
}
Extrude {344, 0, .33} {
  Surface{381, 337};
  Layers{344 / dx + 1};
  Recombine;
}
Line(871) = {347, 353};
Line(872) = {338, 349};
Transfinite Line {871, 872} = cd / dy +1 Using Progression 1;
Line Loop(873) = {871, -851, -872, -831};
Plane Surface(874) = {873};
Transfinite Surface 874;
Extrude {1, 0, 0} {
  Surface{848, 874, 870};
  Layers{1 / dx};
  Recombine;
}
Extrude {0, 0, 20} {
  Surface{891, 905, 935};
  Layers{20 / dz + 1};
  Recombine;
}

Delete {
  Volume{15, 17, 18, 19, 20, 25, 24, 23, 22, 21};
}
Delete {
  Surface{408, 518, 496, 474, 452, 407, 439, 461, 483, 517, 513, 487, 429, 473, 443, 509, 395, 403, 491, 469, 447, 399, 619, 601, 623, 579, 557, 575, 531, 561, 627, 527, 593, 571, 549, 535, 539, 540, 562, 584, 606, 605, 628, 421};
}
Delete {
  Line{547, 570, 591, 622, 556, 526, 574, 600, 618, 530, 525, 534, 523, 542, 545, 586, 611, 610, 609, 589, 588, 565, 544, 520, 521, 522, 564, 566, 398, 394, 442, 468, 486, 508, 512, 482, 459, 438, 393, 402, 500, 477, 457, 433, 388, 390, 499, 478, 434, 389, 391, 432, 476, 501, 456, 454, 415, 413, 411, 416};
}
Delete {
  Point{251, 235, 229, 245, 211, 215, 255, 236, 231, 220, 210, 219, 171, 167, 183, 177, 193, 199, 205, 175, 176, 209, 195, 184, 179, 166};
}
Extrude {5.5, 2.5, 0} {
  Line{182};
  Layers{5.5 / dx / 2 + 1};
}
Extrude {5.5, -2.5, 0} {
  Line{178};
  Layers{5.5 / dx / 2 + 1};
}
Line(1015) = {407, 409};
Line(1016) = {406, 408};
Transfinite Line {1016, 1015} = (dc) Using Progression 1;


Line Loop(1017) = {71, 1012, -1016, -1008};
Plane Surface(1018) = {1017};
Line Loop(1019) = {1013, -1015, -1009, -170};
Plane Surface(1020) = {1019};
Line Loop(1021) = {1015, -1011, -1016, 1007};
Plane Surface(1022) = {1021};
Surface Loop(1023) = {1022, 1020, 1014, 1018, 1010, 183};
Volume(1024) = {1023};

Transfinite Surface "*";
Recombine Surface "*";
Transfinite Volume 1024;

Translate {20.4, 3.5, 0} {
  Duplicata { Point{409}; }
}
Translate {1, 2, 0} {
  Duplicata { Point{410}; }
}
Extrude {0, -4, 0} {
  Point{411};
  Layers{dc - 1};
}
Extrude {-2, 0, 0} {
  Line{1025};
  Layers{dc - 1};
  Recombine;
}

Extrude {20.4-4.3*Cos(Asin(3.5/4.3)), 0, 0} {
  Line{1015};
  Layers{20.4 / dx / 3 + 1};
  Recombine;
}

Translate {4.3*Sin(Acos(3.5/4.3)), 3.5, 0} {
  Duplicata { Point{410}; }
}
Translate {0, -7, 0} {
  Duplicata { Point{417}; }
}
Circle(1034) = {415, 410, 417};
Circle(1035) = {417, 410, 418};
Circle(1036) = {418, 410, 416};
Line(1037) = {415, 413};
Line(1038) = {411, 417};
Line(1039) = {412, 418};
Line(1040) = {414, 416};

Transfinite Line {1034, 1035, 1036} = (dc) Using Progression 1;
Transfinite Line {1037, 1038, 1039, 1040} = (3) Using Progression 1;

Line Loop(1041) = {1030, -1040, -1026, -1037};
Plane Surface(1042) = {1041};
Line Loop(1043) = {1037, -1027, 1038, -1034};
Plane Surface(1044) = {1043};
Line Loop(1045) = {1025, 1039, -1035, -1038};
Plane Surface(1046) = {1045};
Line Loop(1047) = {1039, 1036, -1040, -1028};
Plane Surface(1048) = {1047};

Transfinite Surface "*";
Recombine Surface "*";

Extrude {0, 0, -3.2} {
  Surface{1042, 1044, 1029, 1046, 1048};
  Layers{3.2 / dz + 1};
  Recombine;
}


Line(1159) = {406, 419};
Line(1160) = {408, 420};
Transfinite Line {1159, 1160} = (20.4 / dx / 3 + 1 + 1) Using Progression 1;

Line Loop(1161) = {1159, -1055, -1031, -1007};
Plane Surface(1162) = {1161};
Line Loop(1163) = {1011, 1032, 1056, -1160};
Plane Surface(1164) = {1163};
Line Loop(1165) = {1160, -1050, -1159, 1016};
Plane Surface(1166) = {1165};

Transfinite Surface "*";
Recombine Surface "*";

Surface Loop(1167) = {1162, 1166, 1164, 1033, 1022, 1057};
Volume(1168) = {1167};
Transfinite Volume 1168;


Extrude {0, 0, -7.47-9.1} {
  Surface{78, 62, 66, 70, 74, 1018, 1166, 1158, 1136, 1114, 1070, 1092};
  Layers{(-7.47-9.1) / dz / 2 + 1};
  Recombine;
}

Extrude {0, 0, -17.47+4.5+7.47} {
  Surface{1190, 1212, 1234, 1256, 1278};
  Layers{(-17.47+4.5+7.47) / dz + 1};
  Recombine;
}


Extrude {-2.6, 1.25, 0} {
  Point{550};
  Layers{2.6 / dx * 2 + 1};
}
Extrude {0, -4.5, 0} {
  Point{583};
  Layers{dc - 1};
}
Line(1545) = {584, 546};
Transfinite Line {1545} = (2.6 / dx * 2 + 1 + 1) Using Progression 1;

Line Loop(1546) = {1544, 1545, 1436, 1543};
Plane Surface(1547) = {1546};

Transfinite Surface "*";
Recombine Surface "*";

Extrude {-20, 0, 0} {
  Line{1544};
  Layers{20 / dx * 2 + 1};
  Recombine;
}

Extrude {0, 0, -4.5} {
  Surface{1551, 1547, 1454, 1476, 1498, 1520, 1542};
  Layers{4.5 / dz + 1};
  Recombine;
}
Extrude {0, 0, -1} {
  Surface{1617, 1639, 1661, 1683, 1705};
  Layers{1 / dz / 2 + 1};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{100, 122, 144, 166, 188, 1020, 1033, 1042, 1029, 1046, 1044, 1048};
  Layers{10 / dz + 1};
  Recombine;
}

Physical Surface("inlet") = {1568};
Physical Surface("atmosphere") = {1837, 1859, 1881, 1903, 1925, 1947, 1969, 1991, 2057, 2013, 2079, 2035, 799, 777, 760, 738, 716, 962, 984, 1006};
Physical Surface("outlet") = {826};
Physical Volume("mesh") = {1186, 1197, 1192, 1196, 1191, 1195, 1190, 1194, 1189, 1193, 1188, 1187, 1181, 1182, 1183, 1184, 1185, 1176, 1179, 1178, 1180, 1177, 1029, 1025, 1027, 1026, 1207, 1209, 1205, 1206, 1208, 1028, 1175, 1168, 1204, 1174, 1173, 1172, 1171, 1170, 1169, 1, 2, 3, 4, 5, 1024, 1203, 1202, 1201, 1200, 1199, 1198, 6, 7, 9, 8, 11, 10, 14, 13, 12, 28, 26, 27, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 42, 41};
