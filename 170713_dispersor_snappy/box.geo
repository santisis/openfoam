f = 10;

x0 = -20.0;
x1 = 18.8;
y0 = -5.54;
y1 = 0.03;
z0 = -6.45;
z1 = 0.0;

Point(1) = {0, y0, z0, 1.0};

Extrude {x1, 0, 0} {
  Point{1};
}
Extrude {x0, 0, 0} {
  Point{1};
}

Transfinite Line {1} = Ceil(x1*f/10.) Using Progression 1.2;
Transfinite Line {2} = Ceil(-x0*f/10.) Using Progression 1.2;

Extrude {0, y1-y0, 0} {
  Line{2, 1};
  Layers{Ceil((y1-y0)*f)};
  Recombine;
}
Extrude {0, 0, z1-z0} {
  Surface{6, 10};
  Layers{Ceil((z1-z0)*f)};
  Recombine;
}
