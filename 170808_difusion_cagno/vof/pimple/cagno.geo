r = 1.25;
l = 150;

Point(1) = {0, r/2, r/2, 1.0};
Extrude {0, 0, -r} {
  Point{1};
  Layers{9};
  Recombine;
}
Extrude {0, -r, 0} {
  Line{1};
  Layers{9};
  Recombine;
}

Point(5) = {0, r*Sin(Pi/2), r*Sin(Pi/2), 1.0};
Point(6) = {0, r*Sin(Pi/2), -r*Sin(Pi/2), 1.0};
Point(7) = {0, -r*Sin(Pi/2), -r*Sin(Pi/2), 1.0};
Point(8) = {0, -r*Sin(Pi/2), r*Sin(Pi/2), 1.0};
Line(6) = {1, 5};
Line(7) = {2, 6};
Line(8) = {4, 7};
Line(9) = {3, 8};

Transfinite Line{6,7,8,9} = 10 Using Progression 0.9;

Point(9) = {0, 0, 0, 1.0};
Circle(10) = {8, 9, 5};
Circle(11) = {5, 9, 6};
Circle(12) = {6, 9, 7};
Circle(13) = {7, 9, 8};

Line Loop(14) = {10, -6, 3, 9};
Plane Surface(15) = {14};
Line Loop(16) = {9, -13, -8, -2};
Plane Surface(17) = {16};
Line Loop(18) = {12, -8, -4, 7};
Plane Surface(19) = {18};
Line Loop(20) = {7, -11, -6, 1};
Plane Surface(21) = {20};

Transfinite Line {10,11,12,13} = 10 Using Progression 1;

Transfinite Surface{15,17,19,21};
Recombine Surface{15,17,19,21};

Extrude {l, 0, 0} {
  Surface{5, 15, 17, 19, 21};
  Layers{l};
  Recombine;
}

Physical Surface("inlet") = {5, 15, 21, 19, 17};
Physical Surface("walls") = {122, 96, 52, 38, 78};
Physical Surface("outlet") = {65, 43, 131, 109, 87};
Physical Volume("mesh") = {3, 1, 4, 5, 2};
