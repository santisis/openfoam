r = 1.25;
l0 = 10;
l1 = 90;
r0 = r*2/3;
r1 = r0/9;


Point(1) = {0, r1/2, r1/2, 1.0};
Extrude {0, 0, -r1} {
  Point{1};
  Layers{1};
  Recombine;
}
Extrude {0, -r1, 0} {
  Line{1};
  Layers{1};
  Recombine;
}



Point(5) = {0, r*Sin(Pi/4), r*Sin(Pi/4), 1.0};
Point(6) = {0, r*Sin(Pi/4), -r*Sin(Pi/4), 1.0};
Point(7) = {0, -r*Sin(Pi/4), -r*Sin(Pi/4), 1.0};
Point(8) = {0, -r*Sin(Pi/4), r*Sin(Pi/4), 1.0};
Point(9) = {0, 0, 0, 1.0};

Extrude {0, r0/2-r1/2, 0} {
  Line{1};
  Layers{4};
  Recombine;
}
Extrude {0, -(r0/2-r1/2), 0} {
  Line{2};
  Layers{4};
  Recombine;
}
Extrude {0, 0, (r0/2-r1/2)} {
  Line{7, 3, 11};
  Layers{4};
  Recombine;
}
Extrude {0, 0, -(r0/2-r1/2)} {
  Line{8, 4, 12};
  Layers{4};
  Recombine;
}

Point(26) = {0, r*Cos(Pi/36), r*Sin(Pi/32), 1.0};
Point(27) = {0, -r*Cos(Pi/36), r*Sin(Pi/32), 1.0};
Point(28) = {0, r*Cos(Pi/36), -r*Sin(Pi/32), 1.0};
Point(29) = {0, -r*Cos(Pi/36), -r*Sin(Pi/32), 1.0};
Point(30) = {0, r*Sin(Pi/36), r*Cos(Pi/32), 1.0};
Point(31) = {0, -r*Sin(Pi/36), r*Cos(Pi/32), 1.0};
Point(32) = {0, r*Sin(Pi/36), -r*Cos(Pi/32), 1.0};
Point(33) = {0, -r*Sin(Pi/36), -r*Cos(Pi/32), 1.0};

/*
Line Loop(14) = {10, -6, 3, 9};
Plane Surface(15) = {14};
Line Loop(16) = {9, -13, -8, -2};
Plane Surface(17) = {16};
Line Loop(18) = {12, -8, -4, 7};
Plane Surface(19) = {18};
Line Loop(20) = {7, -11, -6, 1};
Plane Surface(21) = {20};

Transfinite Line {10,11,12,13} = 10 Using Progression 1;

Transfinite Surface{15,17,19,21};
Recombine Surface{15,17,19,21};

Extrude {l, 0, 0} {
  Surface{5, 15, 17, 19, 21};
  Layers{l};
  Recombine;
}

Physical Surface("inlet") = {5, 15, 21, 19, 17};
Physical Surface("walls") = {122, 96, 52, 38, 78};
Physical Surface("outlet") = {65, 43, 131, 109, 87};
Physical Volume("mesh") = {3, 1, 4, 5, 2};
*/
Circle(38) = {6, 9, 28};
Circle(39) = {28, 9, 26};
Circle(40) = {26, 9, 5};
Circle(41) = {5, 9, 30};
Circle(42) = {30, 9, 31};
Circle(43) = {31, 9, 8};
Circle(44) = {8, 9, 27};
Circle(45) = {27, 9, 29};
Circle(46) = {29, 9, 7};
Circle(47) = {7, 9, 33};
Circle(48) = {33, 9, 32};
Circle(49) = {32, 9, 6};
Line(50) = {6, 21};
Line(51) = {28, 11};
Line(52) = {26, 10};
Line(53) = {5, 15};
Line(54) = {30, 14};
Line(55) = {31, 17};
Line(56) = {8, 19};
Line(57) = {27, 12};
Line(58) = {29, 13};
Line(59) = {7, 25};
Line(60) = {33, 23};
Line(61) = {32, 20};
Line Loop(62) = {55, -18, -54, 42};
Plane Surface(63) = {62};
Line Loop(64) = {54, 14, -53, 41};
Plane Surface(65) = {64};
Line Loop(66) = {40, 53, -16, -52};
Plane Surface(67) = {66};
Line Loop(68) = {52, 6, -51, 39};
Plane Surface(69) = {68};
Line Loop(70) = {38, 51, 28, -50};
Plane Surface(71) = {70};
Line Loop(72) = {49, 50, -26, -61};
Plane Surface(73) = {72};
Line Loop(74) = {61, 30, -60, 48};
Plane Surface(75) = {74};
Line Loop(76) = {60, 34, -59, 47};
Plane Surface(77) = {76};
Line Loop(78) = {59, -36, -58, 46};
Plane Surface(79) = {78};
Line Loop(80) = {45, 58, -10, -57};
Plane Surface(81) = {80};
Line Loop(82) = {44, 57, 24, -56};
Plane Surface(83) = {82};
Line Loop(84) = {56, -22, -55, 43};
Plane Surface(85) = {84};

Transfinite Line {56, 55, 54, 53, 52, 51, 50, 61, 60, 59, 58, 57} = 15 Using Progression 1/0.9;
Transfinite Line {44, 43, 41, 40, 38, 49, 47, 46} = 5 Using Progression 1;
Transfinite Line {45, 42, 39, 48} = 1 Using Progression 1;

Transfinite Surface "*";
Recombine Surface "*";
Extrude {-l0, 0, 0} {
  Surface{65, 63, 85, 83, 81, 79, 77, 75, 73, 71, 69, 67, 25, 13, 37, 33, 29, 9, 17, 21};
  Layers{l0};
  Recombine;
}

Extrude {l1, 0, 0} {
  Surface{65, 63, 85, 83, 81, 79, 77, 75, 73, 71, 69, 67, 25, 13, 37, 33, 29, 9, 17, 21, 5};
  Layers{l1};
  Recombine;
}

Recombine Volume "*";
Physical Surface("injector") = {5};
Physical Surface("pipe") = {468, 512, 380, 424};
Physical Surface("inlet") = {129, 151, 173, 195, 217, 239, 261, 283, 305, 327, 349, 107, 525, 371, 393, 415, 437, 459, 481, 503};
Physical Surface("outlet") = {767, 789, 547, 569, 591, 613, 635, 657, 679, 701, 723, 745, 943, 965, 811, 833, 855, 877, 899, 921, 987};
Physical Volume("mesh") = {32, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 37, 39, 38, 41, 40, 33, 34, 35, 36, 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 17, 18, 19, 20, 13, 14, 15, 16};
