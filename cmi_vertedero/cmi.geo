//c01 = 17.3;
c01 = 23;
c00 = -18.47;
c0 = 9.08;
r0 = 16.5;
hc = 3.2;
l1 = 20;
l0 = 276-l1/2;
c1 = 9.21;
l2 = 171-l1/2;
c2 = 9.3;
cv = 13.5;
wc = 8;
cci = 10.63;
ccf = 10.88;
cl = 500;

dx = .5;
dy = .5;
dz = .2;

Point(1) = {0, 0, c0, 1.0};

Extrude {-r0, 0, 0} {
  Point{1};
  Layers{r0 / dx + 1};
  Recombine;
}

Extrude {l0, 0, c1-c0} {
  Point{1};
  Layers{l0 / dx};
  Recombine;
}

Extrude {(l1-wc)/2, 0, 0} {
  Point{3};
  Layers{(l1-wc)/2 / dx};
  Recombine;
}
Extrude {wc, 0, 0} {
  Point{4};
  Layers{wc / dx};
  Recombine;
}
Extrude {(l1-wc)/2, 0, 0} {
  Point{5};
  Layers{(l1-wc)/2 / dx};
  Recombine;
}
Extrude {l2, 0, c2-c1} {
  Point{6};
  Layers{l2 / dx};
  Recombine;
}
Extrude {0, 0, hc} {
  Line{1, 2, 4, 3, 5, 6};
  Layers{hc / dz};
  Recombine;
}
Extrude {0, wc, 0} {
  Surface{10, 14, 22, 18, 26, 30};
  Layers{wc / dy};
  Recombine;
}
Extrude {0, 0, cv-hc-c1} {
  Surface{91, 113, 135};
  Layers{(cv-hc-c1) / dz};
  Recombine;
}
Extrude {0, 0, 10} {
  Surface{184, 206, 228};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {0, .2, 0} {
  Surface{245, 267, 289};
  Layers{Ceil(.2 / dy)};
  Recombine;
}
Extrude {0, 4, 0} {
  Surface{360, 338, 316};
  Layers{4 / dy};
  Recombine;
}
Extrude {0, 0, -.2} {
  Surface{369, 391, 413};
  Layers{Ceil(.2 / dz)};
  Recombine;
}
Extrude {0, 0, -hc} {
  Surface{448, 470, 492};
  Layers{hc / dz};
  Recombine;
}
Extrude {0, cl-8, ccf - cci} {
  Surface{531};
  Layers{(cl-8) / dy};
  Recombine;
}
Extrude {0, -(r0-wc)/2, 0} {
  Surface{10};
  Layers{(r0-wc)/2 / dy};
  Recombine;
}
Extrude {0, (r0-wc)/2, 0} {
  Surface{52};
  Layers{(r0-wc)/2 / dy};
  Recombine;
}
Extrude {0, 0, c00-c0} {
  Surface{589, 39, 611};
  Layers{(c00-c0) / dz / 2};
  Recombine;
}
Extrude {0, 0, c01-(hc+c0)} {
  Surface{597, 47, 619};
  Layers{(c01-(hc+c0)) / dz * 5};
  Recombine;
}

Extrude {0, 4, 0} {
  Surface{580};
  Layers{4/dy};
  Recombine;
}
Extrude {0, 0, 15.9-9.39-3.2-.2 -.8} {
  Surface{765};
  Layers{(15.9-9.39-3.2-.2 - .8) / dz};
  Recombine;
}

Extrude {0, 0, 10} {
  Surface{800};
  Layers{10 / dz * 5};
  Recombine;
}
Extrude {(l1-wc)/2, 0, 0} {
  Surface{821, 799, 777};
  Layers{(l1 - wc) / 2 / dx};
  Recombine;
}

Extrude {-(l1-wc)/2, 0, 0} {
  Surface{813, 791, 769};
  Layers{(l1 - wc) / 2 / dx};
  Recombine;
}

Extrude {0, .2, 0} {
  Surface{901, 817, 843};
  Layers{Ceil(.2 / dy)};
  Recombine;
}
Extrude {0, 4, 0} {
  Surface{976, 998, 1020};
  Layers{4 / dy};
  Recombine;
}

Extrude {1, 0, 0} {
  Surface{153};
  Layers{1 / dx};
  Recombine;
}
Extrude {0, 0, 20} {
  Surface{1099};
  Layers{20 / dz};
  Recombine;
}

Extrude {0, 0, -2} {
  Surface{1077, 1051, 1041};
  Layers{2 / dz};
  Recombine;
}
Extrude {0, 0, -2} {
  Surface{1152, 1174, 1196};
  Layers{2 / dz};
  Recombine;
}

Physical Surface("inlet") = {646, 690, 668};
Physical Surface("outlet") = {1213, 1235, 1257};
Physical Surface("atmosphere") = {712, 734, 756, 421, 311, 250, 272, 333, 399, 377, 355, 294, 905, 822, 839, 967, 993, 1019, 1033, 1059, 1085, 1130};
Physical Surface("walls") = {711, 733, 755, 593, 43, 615, 637, 659, 681, 641, 685, 689, 667, 645, 601, 602, 707, 703, 725, 623, 747, 624, 751, 14, 69, 61, 74, 30, 149, 157, 162, 567, 571, 579, 575, 127, 26, 18, 22, 171, 193, 215, 237, 259, 281, 241, 175, 315, 425, 491, 487, 426, 404, 465, 443, 382, 553, 509, 514, 536, 558, 557, 505, 373, 439, 303, 325, 347, 351, 293, 227, 96, 118, 140, 501, 523, 545, 83, 105, 127, 140, 223, 118, 201, 96, 179, 303, 325, 347, 501, 435, 457, 523, 545, 479, 787, 795, 778, 773, 941, 875, 887, 945, 954, 888, 883, 949, 865, 795, 923, 932, 931, 787, 857, 866, 909, 809, 835, 844, 910, 1015, 1081, 1086, 1064, 1042, 1037, 971, 975, 985, 1011, 1117, 1125, 1129, 1121, 1103, 1095, 1108, 1107, 1218, 1240, 1262, 1205, 1227, 1249, 1209, 1261, 1143, 1139, 1161, 1183, 1195, 1191, 1169, 1147};
Physical Volume("mesh") = {28, 29, 30, 26, 1, 27, 31, 32, 33, 2, 6, 25, 5, 4, 3, 7, 10, 11, 8, 9, 12, 22, 19, 16, 23, 20, 15, 24, 14, 21, 17, 18, 13, 34, 35, 39, 42, 41, 38, 40, 36, 37, 43, 44, 45, 46, 47, 48, 50, 49, 54, 51, 55, 52, 56, 53};
