pc = 10;
//pch = pc*4;
pch = pc*2;
//dx = 0.5;
dx = 1;
//dz = 0.1;
dz = 0.2;
dm = (.5 + (1.5-1.5*Sin(Pi/4))*2) / dz/2;

Point(1) = {0, 2, 10.68, 1.0};
Translate {0, 1.5*Cos(Pi/4), 1.5*Cos(Pi/4)} {
  Duplicata { Point{1}; }
}
Translate {0, -1.5*Cos(Pi/4), 1.5*Cos(Pi/4)} {
  Duplicata { Point{1}; }
}
Translate {0, -1.5*Cos(Pi/4), -1.5*Cos(Pi/4)} {
  Duplicata { Point{1}; }
}
Translate {0, 1.5*Cos(Pi/4), -1.5*Cos(Pi/4)} {
  Duplicata { Point{1}; }
}
Circle(1) = {2, 1, 5};
Circle(2) = {5, 1, 4};
Circle(3) = {4, 1, 3};
Circle(4) = {3, 1, 2};

Translate {0, .5, .5} {
  Duplicata { Point{1}; }
}
Translate {0, .5, -.5} {
  Duplicata { Point{1}; }
}
Translate {0, -.5, -.5} {
  Duplicata { Point{1}; }
}
Translate {0, -.5, .5} {
  Duplicata { Point{1}; }
}

Line(5) = {6, 9};
Line(6) = {9, 8};
Line(7) = {8, 7};
Line(8) = {7, 6};
Line(9) = {6, 2};
Line(10) = {9, 3};
Line(11) = {8, 4};
Line(12) = {7, 5};

Line Loop(13) = {9, 1, -12, 8};
Plane Surface(14) = {13};
Line Loop(15) = {5, 6, 7, 8};
Plane Surface(16) = {15};
Line Loop(17) = {9, -4, -10, -5};
Plane Surface(18) = {17};
Line Loop(19) = {6, 11, 3, -10};
Plane Surface(20) = {19};
Line Loop(21) = {7, 12, 2, -11};
Plane Surface(22) = {21};

Translate {0, 3.5, 0} {
  Duplicata { Surface{18, 14, 16, 22, 20}; }
}
Translate {0, 3.5, 0} {
  Duplicata { Surface{33, 28, 23, 43, 38}; }
}

Transfinite Line{1,2,3,4,27,32,35,36,48,51,49,50} = pc + 1;
Transfinite Line{5,6,7,8,25,46,41,30,59,65,70,54} = pc + 1;
Transfinite Line{1,3,8,6,46,35,32,30,65,49,51,54} = pch + 1;
Transfinite Line{9,10,11,12,24,-26,-31,-42,53,-60,-55,64} = 25 + 1 Using Progression 1 / 1.1;
Transfinite Surface "*";
Recombine Surface "*";

Extrude {311.2, 0, 9.41-9.18} {
  Surface{47, 52, 57, 62, 67, 33, 28, 23, 43, 38, 14, 16, 18, 20, 22};
  Layers{311.2/dx+1};
  Recombine;
}

Line(401) = {188, 180};
Line(402) = {193, 186};
Line(403) = {173, 156};
Line(404) = {168, 150};
Line Loop(405) = {401, -250, -402, -293};
Plane Surface(406) = {405};
Line Loop(407) = {404, -140, -403, -205};
Plane Surface(408) = {407};
Translate {0, 0, -(1.5-1.5*Sin(Pi/4)+.5)} {
  Duplicata { Point{216, 193, 186, 173, 156, 143}; }
}

Line(409) = {217, 216};
Line(410) = {217, 218};
Line(411) = {218, 193};
Line(412) = {218, 219};
Line(413) = {219, 186};
Line(414) = {219, 220};
Line(415) = {220, 173};
Line(416) = {220, 221};
Line(417) = {221, 156};
Line(418) = {221, 222};
Line(419) = {222, 143};

Line Loop(420) = {409, -382, -411, -410};
Plane Surface(421) = {420};
Line Loop(422) = {412, 413, -402, -411};
Plane Surface(423) = {422};
Line Loop(424) = {414, 415, 272, -413};
Plane Surface(425) = {424};
Line Loop(426) = {415, 403, -417, -416};
Plane Surface(427) = {426};
Line Loop(428) = {417, -162, -419, -418};
Plane Surface(429) = {428};

Extrude {0, -(1.5-1.5*Sin(Pi/4)+2.5), 0} {
  Point{217};
}
Extrude {0, (1.5-1.5*Sin(Pi/4)+2.5), 0} {
  Line{419};
}
Extrude {0, (1.5-1.5*Sin(Pi/4)+2.5), 0} {
  Point{138};
}
Line(436) = {226, 225};
Extrude {0, 0, .5} {
  Point{223};
}
Extrude {0, 0, 3.2} {
  Point{227};
}
Line(439) = {210, 228};
Line(440) = {216, 227};
Line Loop(441) = {438, -439, -360, 440};
Plane Surface(442) = {441};
Line Loop(443) = {440, -437, -430, 409};
Plane Surface(444) = {443};
Line Loop(445) = {435, 436, -433, -95};
Plane Surface(446) = {445};
Extrude {0, 0, .5} {
  Point{228};
}
Extrude {0, 0, 1.5-1.5*Sin(Pi/4)+.7} {
  Point{210, 188, 180, 168, 150, 138, 226};
}
Line(455) = {229, 230};
Line(456) = {230, 231};
Line(457) = {231, 232};
Line(458) = {232, 233};
Line(459) = {233, 234};
Line(460) = {234, 235};
Line(461) = {235, 236};
Line Loop(462) = {455, -448, 439, 447};
Plane Surface(463) = {462};
Line Loop(464) = {448, 456, -449, 337};
Plane Surface(465) = {464};
Line Loop(466) = {449, 457, -450, -401};
Plane Surface(467) = {466};
Line Loop(468) = {450, 458, -451, 227};
Plane Surface(469) = {468};
Line Loop(470) = {451, 459, -452, -404};
Plane Surface(471) = {470};
Line Loop(472) = {452, 460, -453, 117};
Plane Surface(473) = {472};
Line Loop(474) = {453, 461, -454, -435};
Plane Surface(475) = {474};

Transfinite Line{409,411,413,415,417,419,437,431} = .5/dz+1;
Transfinite Line{410,414,418,456,458,460} = pc+1;
Transfinite Line{401,402,412,404,403,416,457,459} = dm+1;
Transfinite Line{447,448,449,450,451,452,453,454} = .7/dz+1;
Transfinite Surface "*";
Recombine Surface "*";

Transfinite Line{436} = pch+1;
Transfinite Line{438} = pch+1 Using Bump .1;
Transfinite Line{455,439,440,430,461,435,433,432} = 1.5 / dz;
Transfinite Surface "*";
Recombine Surface "*";

Extrude {3.25, 0, 0} {
  Surface{442, 463, 465, 356, 378, 334, 312, 400, 421, 444, 406, 467, 423, 425, 290, 268, 246, 469, 202, 224, 471, 408, 427, 158, 136, 473, 92, 180, 429, 114, 475, 446, 434};
  Layers{3.25/dz+1};
  Recombine;
}
Extrude {4, 0, 0} {
  Surface{497, 519, 695, 673, 651, 585, 607, 563, 739, 541, 629, 717, 761, 827, 849, 871, 893, 805, 783, 915, 937, 959, 981, 1003, 1025, 1069, 1047, 1135, 1091, 1113, 1201, 1179, 1157};
  Layers{pch*4/3.2};
  Recombine;
}

Extrude {.5, 0, 0} {
  Surface{1223, 1245, 1333, 1421, 1377, 1355, 1311, 1267, 1289, 1487, 1465, 1443, 1575, 1399, 1509, 1531, 1553, 1597, 1619, 1641, 1663, 1685, 1707, 1839, 1861, 1729, 1773, 1751, 1795, 1817, 1927, 1905, 1883};
  Layers{.5/dz+1};
  Recombine;
}
Extrude {4, 0, 0} {
  Surface{1949, 1971, 1993, 2015, 2037, 2059, 2081, 2125, 2103, 2191, 2235, 2257, 2147, 2169, 2301, 2279, 2213, 2323, 2367, 2345, 2433, 2411, 2389, 2565, 2543, 2499, 2477, 2455, 2521, 2587, 2631, 2609, 2653};
  Layers{pch*4/3.2};
  Recombine;
}
Extrude {3.25, 0, 0} {
  Surface{2697, 2675, 2851, 2829, 2807, 2719, 2763, 2785, 2741, 2873, 2895, 2961, 2939, 3093, 3071, 2917, 3027, 3049, 3005, 2983, 3159, 3137, 3115, 3247, 3269, 3225, 3181, 3203, 3291, 3313, 3357, 3335, 3379};
  Layers{3.25/dz+1};
  Recombine;
}

Delete {
  Volume{49, 115, 51, 123, 50, 116};
}
Delete {
  Surface{1210, 2662, 1222, 2674, 1214, 2666};
}
Delete {
  Surface{1258, 2842, 1244, 2696};
}
Delete {
  Line{1208, 2660, 1209, 2661};
}
Line(4106) = {238, 430};
Line(4107) = {600, 787};
Line(4108) = {237, 429};
Line(4109) = {599, 786};
Transfinite Line{4106,4107,4108,4109} = pch*4/3.2+1 Using Bump .1;
Line Loop(4110) = {4108, 1248, -1257, -676};
Plane Surface(4111) = {4110};
Line Loop(4112) = {4108, -1206, -1217, 480};
Plane Surface(4113) = {4112};
Line Loop(4114) = {4109, 2832, -2841, -2084};
Plane Surface(4115) = {4114};
Line Loop(4116) = {2658, -4109, -1932, 2669};
Plane Surface(4117) = {4116};
Line Loop(4118) = {4106, 1228, -1230, -502};
Plane Surface(4119) = {4118};
Line Loop(4120) = {4106, 1204, -1213, -478};
Plane Surface(4121) = {4120};
Line Loop(4122) = {1930, 2665, -2656, -4107};
Plane Surface(4123) = {4122};
Line Loop(4124) = {2680, -2682, -1954, 4107};
Plane Surface(4125) = {4124};
Line Loop(4126) = {1203, -4106, -477, 4108};
Plane Surface(4127) = {4126};
Line Loop(4128) = {1929, 4107, -2655, -4109};
Plane Surface(4129) = {4128};

Transfinite Surface "*";
Recombine Surface "*";
Surface Loop(4130) = {2846, 4115, 4117, 2816, 2851, 2103};
Volume(4131) = {4130};
Surface Loop(4132) = {1262, 4111, 695, 1266, 4113, 1267};
Volume(4133) = {4132};
Surface Loop(4134) = {4117, 4129, 2675, 1949, 2670, 4123};
Volume(4135) = {4134};
Surface Loop(4136) = {4113, 4127, 497, 1223, 1218, 4121};
Volume(4137) = {4136};
Surface Loop(4138) = {1232, 4119, 4121, 1236, 519, 1245};
Volume(4139) = {4138};
Surface Loop(4140) = {4125, 2684, 2688, 4123, 2697, 1971};
Volume(4141) = {4140};

Transfinite Volume {4131,4133,4135,4137,4139,4141};
Recombine Volume {4131,4133,4135,4137,4139,4141};

Extrude {0, -43, 11-9.41} {
  Surface{4127, 4129};
  Layers{43/dx+1};
  Recombine;
}

Line(14150) = {1152, 1162};
Line(14151) = {1151, 1161};
Line Loop(14152) = {4143, 14150, -4165, -14151};
Plane Surface(14153) = {14152};

Transfinite Line{14150,14151} = .5/dz+1;
Transfinite Surface "*";
Recombine Surface "*";

Extrude {0, -7, 0} {
  Surface{4163, 14153, 4185};
  Layers{7/dz+1};
  Recombine;
}


Extrude {5, 0, 0} {
  Surface{14214};
  Layers{5/dz+1};
  Recombine;
}

Line(14242) = {110, 11};
Line(14243) = {16, 2};
Line(14244) = {126, 33};
Line(14245) = {70, 5};
Line Loop(14246) = {14244, -30, -14242, -65};
Plane Surface(14247) = {14246};
Line Loop(14248) = {46, 14243, 1, -14245};
Plane Surface(14249) = {14248};

Point(11207) = {0, 0, 10.68, 1.0};
Delete {
	Point{11207};
}

Extrude {0, -20, 0} {
  Point{3, 4};
}
Line(14252) = {11208, 11207};


Extrude {0, 20, 0} {
  Point{93, 88};
}

Line(14256) = {11210, 11209};

Line Loop(14257) = {14253, -14256, -14254, 54};
Plane Surface(14258) = {14257};
Line Loop(14259) = {3, 14250, -14252, -14251};
Plane Surface(14260) = {14259};
Extrude {0, 0, 14-11.74} {
  Point{11210, 88, 110, 11, 16, 2, 3, 11207};
}

Line(14269) = {11211, 11212};
Line(14270) = {11212, 11213};
Line(14271) = {11213, 11214};
Line(14272) = {11214, 11215};
Line(14273) = {11215, 11216};
Line(14274) = {11216, 11217};
Line(14275) = {11217, 11218};

Line Loop(14276) = {14269, -14262, 14254, 14261};
Plane Surface(14277) = {14276};
Line Loop(14278) = {14270, -14263, -59, 14262};
Plane Surface(14279) = {14278};
Line Loop(14280) = {14271, -14264, -14242, 14263};
Plane Surface(14281) = {14280};
Line Loop(14282) = {14272, -14265, -25, 14264};
Plane Surface(14283) = {14282};
Line Loop(14284) = {14273, -14266, -14243, 14265};
Plane Surface(14285) = {14284};
Line Loop(14286) = {4, 14266, 14274, -14267};
Plane Surface(14287) = {14286};
Line Loop(14288) = {14267, 14275, -14268, -14250};
Plane Surface(14289) = {14288};

Extrude {0, 0, -(1.5-1.5*Sin(Pi/4)+.5)} {
  Point{11209, 93, 126, 33, 70, 5, 4, 11208};
}

Line(14298) = {11219, 11220};
Line(14299) = {11220, 11221};
Line(14300) = {11221, 11222};
Line(14301) = {11222, 11223};
Line(14302) = {11223, 11224};
Line(14303) = {11224, 11225};
Line(14304) = {11225, 11226};
Line Loop(14305) = {14298, -14291, 14253, 14290};
Plane Surface(14306) = {14305};
Line Loop(14307) = {14299, -14292, -70, 14291};
Plane Surface(14308) = {14307};
Line Loop(14309) = {14292, 14300, -14293, -14244};
Plane Surface(14310) = {14309};
Line Loop(14311) = {14293, 14301, -14294, -41};
Plane Surface(14312) = {14311};
Line Loop(14313) = {14294, 14302, -14295, -14245};
Plane Surface(14314) = {14313};
Line Loop(14315) = {2, 14296, -14303, -14295};
Plane Surface(14316) = {14315};
Line Loop(14317) = {14304, -14297, -14251, 14296};
Plane Surface(14318) = {14317};

Transfinite Line{14270,14272,14274,14299,14301,14303} = pc+1;
Transfinite Line{14252,14256} = pch+1;
Transfinite Surface "*";
Recombine Surface "*";

Extrude {-40, 0, 0} {
  Surface{14277, 14258, 14306, 14308, 67, 52, 47, 57, 14279, 62, 14281, 14247, 14310, 28, 14283, 33, 23, 38, 14312, 14249, 43, 14314, 14285, 14, 14287, 18, 16, 22, 14316, 14260, 14289, 20, 14318};
  Layers{10};
  Recombine;
}


Physical Surface("inlet") = {14340, 14384, 14362, 14516, 14494, 14428, 14472, 14538, 14406, 14582, 14450, 14560, 14604, 14736, 14626, 14670, 14714, 14692, 14648, 14780, 14824, 14758, 14802, 14956, 14934, 14846, 14890, 14868, 14912, 15022, 15000, 14978, 15044};
Physical Surface("outlet") = {14241};
Physical Volume("mesh") = {4144, 4145, 4146, 4147, 4143, 4142, 150, 149, 127, 122, 121, 91, 117, 120, 71, 124, 99, 100, 148, 128, 78, 126, 131, 156, 105, 133, 79, 136, 158, 140, 143, 167, 166, 144, 114, 142, 154, 104, 106, 132, 155, 134, 153, 152, 151, 160, 157, 135, 159, 163, 164, 141, 165, 161, 170, 162, 169, 147, 173, 168, 145, 174, 175, 176, 172, 171, 180, 179, 178, 177, 4131, 90, 4135, 88, 67, 93, 92, 66, 87, 84, 61, 89, 52, 53, 82, 4133, 25, 16, 20, 23, 24, 4137, 28, 54, 29, 30, 55, 59, 38, 60, 4141, 44, 96, 119, 86, 62, 95, 65, 43, 85, 39, 57, 37, 35, 58, 56, 83, 31, 34, 26, 4139, 22, 21, 17, 19, 18, 27, 33, 32, 36, 41, 46, 40, 42, 64, 45, 63, 68, 118, 70, 48, 97, 94, 77, 103, 101, 125, 74, 98, 47, 69, 75, 73, 102, 76, 129, 130, 108, 111, 139, 113, 138, 109, 80, 137, 110, 81, 112, 146, 107, 72, 3, 2, 1, 4, 5, 8, 7, 6, 9, 10, 13, 11, 12, 15, 14, 4177, 4180, 4178, 4179, 4175, 4173, 4174, 4176, 4169, 4171, 4167, 4172, 4170, 4168, 4165, 4166, 4163, 4162, 4164, 4161, 4160, 4159, 4157, 4152, 4151, 4154, 4155, 4153, 4156, 4158, 4150, 4149, 4148};
