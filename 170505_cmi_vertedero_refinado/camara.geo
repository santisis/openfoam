dx = .1;
dy = .1;
dz = .05;
dc = 2 / dy * 2;

sp4 = Sin(Pi/4);

Point(1) = {0, 0, -7.47, 1.0};
Extrude {13, 0, 0} {
  Point{1};
  Layers{13 / dx + 1};
}
Extrude {0, -2, 0} {
  Line{1};
  Layers{dc};
}
Extrude {5.5, 2.5, 0} {
  Point{2};
  Layers{5.5 / dx + 1};
}
Extrude {0, -7, 0} {
  Point{5};
  Layers{dc};
}
Line(8) = {6, 4};
Transfinite Line {8} = (5.5 / dx + 2) Using Progression 1;
Extrude {13, 0, 0} {
  Line{7};
  Layers{13 / dx + 1}; 
}
Translate {20.41, -3.5, 0} {
  Duplicata { Point{5}; }
}
Translate {-4.7*sp4, 4.7*sp4, 0} {
  Duplicata { Point{9}; }
}
Translate {-4.7*sp4, -4.7*sp4, 0} {
  Duplicata { Point{9}; }
}
Translate {4.7*sp4, -4.7*sp4, 0} {
  Duplicata { Point{9}; }
}
Translate {4.7*sp4, 4.7*sp4, 0} {
  Duplicata { Point{9}; }
}
Circle(13) = {11, 9, 10};
Circle(14) = {10, 9, 13};
Circle(15) = {13, 9, 12};
Circle(16) = {12, 9, 11};
Line(17) = {7, 10};
Line(18) = {8, 11};

Translate {8.2*sp4, 8.2*sp4, 0} {
  Duplicata { Point{9}; }
}
Translate {8.2*sp4, -8.2*sp4, 0} {
  Duplicata { Point{9}; }
}
Circle(19) = {7, 9, 14};
Circle(20) = {14, 9, 15};
Circle(21) = {15, 9, 8};
Line(22) = {13, 14};
Line(23) = {12, 15};
Translate {-1.5, 1.5, 0} {
  Duplicata { Point{9}; }
}
Extrude {3, 0, 0} {
  Point{16};
  Layers{dc};
}
Extrude {0, -3, 0} {
  Line{24};
  Layers{dc};
}
Line(29) = {10, 16};
Line(30) = {13, 17};
Line(31) = {12, 19};
Line(32) = {11, 18};

Transfinite Line {13, 15, 19, 20, 21, 14, 16} = (dc + 1) Using Progression 1;
Transfinite Line {17, 18, 22, 23} = (dc + 1) Using Progression 1;
Transfinite Line {29, 30, 31, 32} = (dc + 1) Using Progression 1;

Line Loop(33) = {4, -8, -7, -6};
Plane Surface(34) = {33};
Line Loop(35) = {13, -17, 9, 18};
Plane Surface(36) = {35};
Line Loop(37) = {14, 22, -19, 17};
Plane Surface(38) = {37};
Line Loop(39) = {15, 23, -20, -22};
Plane Surface(40) = {39};
Line Loop(41) = {21, 18, -16, 23};
Plane Surface(42) = {41};
Line Loop(43) = {25, -31, 16, 32};
Plane Surface(44) = {43};
Line Loop(45) = {31, -27, -30, 15};
Plane Surface(46) = {45};
Line Loop(47) = {30, -24, -29, 14};
Plane Surface(48) = {47};

Transfinite Surface "*";
Recombine Surface "*";

Extrude {0, 0, -5.6 + 7.47} {
  Surface{5, 34, 12, 36, 38, 48, 28, 44, 42, 46, 40};
  Layers{(-5.6+7.47)/dz + 1};
  Recombine;
}
Extrude {0, 0, -0.64+5+5.6} {
  Surface{70, 92, 114, 136, 158, 290, 246};
  Layers{(0.64+5+5.6)/dz + 1};
  Recombine;
}

Extrude {0, 0, -12.97+7.47} {
  Surface{5};
  Layers{(-7.47+12.97)/dz + 1};
  Recombine;
}

Extrude {-2.6, 1.25, 0} {
  Point{130};
  Layers{2.6/dx + 1};
}
Extrude {0, -4.5, 0} {
  Point{140};
  Layers{dc};
}
Line(512) = {139, 141};
Transfinite Line 512 = (2.6 / dx + 2) Using Progression 1;

Line Loop(513) = {468, -512, 449, 467};
Plane Surface(514) = {513};
Transfinite Surface 514;


Extrude {-20, 0, 0} {
  Line{468};
  Layers{20/dx / 3 + 1};
}
Extrude {-27, 0, 0} {
  Line{515};
  Layers{10};
}
Extrude {0, -5, 0} {
  Line{521};
  Layers{5};
}
Extrude {0, 5, 0} {
  Line{520};
  Layers{5};
}

Extrude {0, 0, -4.5} {
  Surface{530, 522, 518, 526, 514, 466};
  Layers{4.5/dz + 1};
  Recombine;
}
Extrude {0, 0, -1} {
  Surface{662};
  Layers{1/dz + 1};
  Recombine;
}
Extrude {0, 0, -.44+12.97} {
  Surface{530, 522, 526};
  Layers{20};
  Recombine;
}

Transfinite Surface "*";
Recombine Surface "*";

Physical Surface("inlet") = {543, 697, 723, 741, 569, 609};
Physical Surface("outlet") = {180, 268, 224, 202};
Physical Surface("atmosphere") = {312, 334, 356, 378, 400, 422, 444};
Physical Volume("mesh") = {20, 21, 23, 29, 28, 27, 22, 24, 26, 25, 19, 1, 12, 2, 13, 3, 14, 4, 15, 5, 16, 6, 7, 8, 9, 18, 10, 11, 17};
