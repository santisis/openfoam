l = 50;
w = 25;
h = 4;
d = .5;

di = 1;

Point(1) = {0, 0, 0, 1.0};
Extrude {0, 0, h/2-di/2} {
  Point{1};
  Layers{(h/2-di/2 + 1)/ d};
  Recombine;
}
Extrude {0, l, 0} {
  Line{1};
  Layers{(l + 1) / d};
  Recombine;
}
Extrude {w/2-di/2, 0, 0} {
  Surface{5};
  Layers{(w/2-di/2 + 1) / d};
  Recombine;
}
Extrude {di, 0, 0} {
  Surface{27};
  Layers{(di + 1) / d};
  Recombine;
}
Extrude {w/2-di/2, 0, 0} {
  Surface{49};
  Layers{(w/2-di/2 + 1) / d};
  Recombine;
}

Extrude {0, 0, di} {
  Surface{18, 40, 62};
  Layers{(di + 1)/ d};
  Recombine;
}

Extrude {0, 0, h/2-di/2} {
  Surface{93, 115, 137};
  Layers{(h/2-di/2 + 1)/ d};
  Recombine;
}
Physical Surface("discharge") = {114};
Physical Surface("inlet") = {158, 92, 14, 180, 202, 136, 58, 36};
Physical Surface("outlet") = {150, 84, 22, 172, 106, 44, 66, 128, 194};
Physical Volume("river") = {9, 8, 7, 4, 5, 6, 3, 2, 1};
Physical Surface("walls") = {5, 80, 146, 198, 132, 71};
Physical Surface("top") = {159, 181, 203};
Physical Surface("bottom") = {70, 48, 26};
