l = 50;
w = 25;
h = 4;
d = .5;

Point(1) = {0, 0, 0, 1.0};
Extrude {0, 0, h} {
  Point{1};
  Layers{(h + 1)/ d};
  Recombine;
}
Extrude {0, l, 0} {
  Line{1};
  Layers{(l + 1) / d};
  Recombine;
}
Extrude {w/3-1, 0, 0} {
  Surface{5};
  Layers{(w/3-1 + 1) / d};
  Recombine;
}
Extrude {w/3-1, 0, 0} {
  Surface{27};
  Layers{(w/3-1 + 1) / d};
  Recombine;
}
Extrude {w/3-1, 0, 0} {
  Surface{49};
  Layers{(w/3-1 + 1) / d};
  Recombine;
}
Physical Surface("discharge") = {36};
Physical Surface("inlet") = {14, 58};
Physical Surface("outlet") = {22, 44, 66};
Physical Volume("river") = {1, 2, 3};
Physical Surface("topbot") = {18, 40, 62, 70, 48, 26};
Physical Surface("walls") = {5, 71};
