c01 = 17.3;
c00 = -18.47;
c0 = 9.8;
r0 = 16.5;
hc = 3.2;
l0 = 212;
c1 = 9.49;
l1 = 20;
l2 = 139+116;
c2 = 9.3;
cv = 13.5;
wc = 8;

dx = .5;
dy = .5;
dz = .2;

Point(1) = {0, 0, c0, 1.0};

Extrude {-r0, 0, 0} {
  Point{1};
  Layers{r0 / dx + 1};
  Recombine;
}

Point(5) = {l0, 0, c1, 1.0};
Line(6) = {1, 5};
Transfinite Line {6} = (l0 / dx) Using Progression 1;

Point(7) = {l1+l0, 0, c1, 1.0};
Point(8) = {l2+l1+l0, 0, c2, 1.0};
Line(7) = {5, 7};
Transfinite Line {7} = (l1 / dx + 1) Using Progression 1;

Line(8) = {7, 8};
Transfinite Line {8} = (l2 / dx + 1) Using Progression 1;

Extrude {0, 0, hc} {
  Line{1, 6, 8, 7};
  Layers{hc / dz};
  Recombine;
}
Extrude {0, 0, cv-hc-c1} {
  Line{21};
  Layers{Ceil((cv-hc-c1) / dz * 5)};
  Recombine;
}
Extrude {0, 0, 5} {
  Line{25};
  Layers{5 / dz * 5};
  Recombine;
}
Extrude {0, 0, c01-(hc+c0)} {
  Line{9};
  Layers{(c01-(hc+c0)) / dz};
  Recombine;
}
Extrude {0, wc, 0} {
  Surface{36, 12, 16, 24, 28, 32, 20};
  Layers{Ceil(wc) / dy};
  Recombine;
}
Extrude {0, 0, c00-c0} {
  Surface{67};
  Layers{(c00-c0) / dz / 5};
  Recombine;
}
Extrude {0, -(r0-wc)/2, 0} {
  Surface{199, 12, 36};
  Layers{(r0-wc)/2 / dy};
  Recombine;
}
Extrude {0, (r0-wc)/2, 0} {
  Surface{207, 58, 80};
  Layers{(r0-wc)/2 / dy};
  Recombine;
}

Extrude {0, .2, 0} {
  Surface{168};
  Layers{Ceil(.2 / dx)};
  Recombine;
}
Extrude {0, 3, 0} {
  Surface{366};
  Layers{3 / dy};
  Recombine;
}
Extrude {0, 0, -3} {
  Surface{375};
  Layers{3 / dz};
  Recombine;
}

Physical Surface("inlet") = {229, 295};
Physical Surface("outlet") = {405};
Physical Surface("atmosphere") = {163, 317, 53, 273, 361, 383};
Physical Surface("walls") = {212, 225, 203, 299, 234, 199, 207, 300, 233, 211, 291, 247, 269, 71, 49, 313, 335, 256, 322, 344, 321, 57, 277, 89, 16, 102, 97, 255, 343, 111, 24, 124, 28, 32, 167, 145, 137, 159, 146, 177, 20, 190, 185, 181, 278, 353, 357, 365, 387, 388, 379, 409, 397, 410, 401};
Physical Volume("mesh") = {7, 4, 5, 6, 3, 9, 8, 12, 10, 2, 14, 11, 1, 13, 15, 16, 17};
